package com.medic.DAO;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.medic.fftshow.R;

@DatabaseTable(tableName = "patient")
public class Patient {

	
	public static final String FIELD_PATIENT_IDENT_INFO = "patient_ident_info";
	
	public static final String FIELD_ADDITIONAL_INFO = "patient_additional_info";

	public static final String FIELD_SURNAME = "surname";

	public static final String FIELD_NAME = "name";
	
	//Gender, male = true, female = woman
	public static final String FIELD_GENDER = "gender";

	private static final String FIELD_PATIENT_ID = "patient_id";
	
	
	@DatabaseField(generatedId=true, columnName=FIELD_PATIENT_ID)
	private long patient_id;
	
	@DatabaseField(dataType=DataType.STRING,  columnName=FIELD_NAME)
	private String name;
	
	@DatabaseField(dataType=DataType.STRING, columnName=FIELD_SURNAME)
	private String surname; 
	
	@DatabaseField(dataType=DataType.STRING, columnName=FIELD_PATIENT_IDENT_INFO)
	private String patient_ident_info; 
	
	@DatabaseField(dataType=DataType.STRING, columnName=FIELD_ADDITIONAL_INFO)
	private String patient_additional_info; 
	
	@DatabaseField(dataType=DataType.BOOLEAN, columnName=FIELD_GENDER)
	private boolean patient_gender;
	
	

	public Patient() {
		// TODO Auto-generated constructor stub
	}

	public Patient(String patientIdentInfo, 
			String patientName,
			String patientSurname, 
			boolean patientGender ,
			String patient_additional_info) {
		
		this.name = patientName;
		
		this.surname = patientSurname;
		
		this.patient_gender = patientGender;
		
		this.patient_ident_info = patientIdentInfo;
		
		this.patient_additional_info = patient_additional_info;
	}
	
	

}
