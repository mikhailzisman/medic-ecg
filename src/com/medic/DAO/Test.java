package com.medic.DAO;

import java.util.Date;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.medic.fftshow.R;

@DatabaseTable(tableName = "test")
public class Test {
	
	public static final String RESULT = "result";

	public static final String FIELD_MEDIC_ID = "medic_id";

	public static final String FIELD_FILE_PATH = "file_path";

	public static final String FIELD_PACIENT_ID = "pacient_id";

	public static final String FIELD_DATE = "date";
	
	public static final String FIELD_DEVICE_ID = "device_id";

	@DatabaseField(generatedId=true)
	private long id;
	
	@DatabaseField(foreign = true, columnName=FIELD_MEDIC_ID)
	private Medic medic;
	
	@DatabaseField(foreign = true, columnName=FIELD_PACIENT_ID)
	private Patient pacient_id;
	
	@DatabaseField(foreign = true, columnName=FIELD_DEVICE_ID)
	private Device device_id;
	
	@DatabaseField(dataType=DataType.STRING,  columnName=FIELD_FILE_PATH)
	private String file_path;
	
	@DatabaseField(dataType=DataType.STRING,  columnName=RESULT)
	private String result;
	
	@DatabaseField(dataType=DataType.DATE,  columnName=FIELD_DATE)
	private Date date;
	
	public Test(){
		
	}

	public Test(Medic medic, Patient patient, Device device) {
		
		this.medic = medic;
		
		this.pacient_id = patient;
		
		this.device_id = device;
		
	}
	
	

}
