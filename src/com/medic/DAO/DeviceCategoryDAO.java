package com.medic.DAO;

import java.sql.SQLException;
import java.util.List;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;
import com.medic.database.HelperFactory;
import com.medic.fftshow.R;

public class DeviceCategoryDAO extends BaseDaoImpl<DeviceCategory, Integer> {

	public DeviceCategoryDAO(ConnectionSource connectionSource,
			Class<DeviceCategory> dataClass) throws SQLException {
		super(connectionSource, dataClass);
	}
	
	public List<DeviceCategory> getAllDevicesCategories() throws SQLException{
		return this.queryForAll();
	}
	
	public void addCategory(DeviceCategory category) throws SQLException{
		this.createIfNotExists(category);
	}
	
	public void deleteCategory(DeviceCategory category) throws SQLException{
		this.delete(category);
	}
	
	public void updateCategory(DeviceCategory category) throws SQLException{
		this.update(category);
	}

}
