package com.medic.DAO;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.medic.fftshow.R;

@DatabaseTable(tableName = "medic")
public class Medic {
	
	public static final String FIELD_TOKEN_NAME = "token";
	public static final String FIELD_LOGIN_NAME = "login";
	public static final String FIELD_PASSWORD_NAME = "password";
	public static final String FIELD_NAME_NAME = "name";
	public static final String FIELD_SURNAME_NAME = "surname";
	
	@DatabaseField(generatedId=true, dataType=DataType.LONG)
	private long medic_id;
	
	@DatabaseField(dataType=DataType.STRING, columnName=FIELD_TOKEN_NAME)
	private String token; 
	
	@DatabaseField(dataType=DataType.STRING, canBeNull=false, columnName=FIELD_LOGIN_NAME)
	private String login;
	
	@DatabaseField(dataType=DataType.STRING, canBeNull=false, columnName=FIELD_PASSWORD_NAME)
	private String password;
	
	@DatabaseField(dataType=DataType.STRING, columnName=FIELD_NAME_NAME)
	private String name;
	
	@DatabaseField(dataType=DataType.STRING, columnName=FIELD_SURNAME_NAME)
	private String surname; 		

	public Medic() {
		
	}

	public Medic(String login, String password, String name, String surname) {
		this.login = login;
		this.password = password;
		this.name = name; 
		this.surname = surname;
	}

}
