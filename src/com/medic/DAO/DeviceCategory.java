package com.medic.DAO;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.medic.fftshow.R;

@DatabaseTable(tableName = "device_category")
public class DeviceCategory {
	
	private static final String FIELD_DEVICE_CATEGORY_ID = "device_category_id";

	private static final String FIELD_DEVICE_CATEGORY_NAME = "category_name";
	
	@DatabaseField(id = true, dataType=DataType.LONG)
	private long device_category_id; 
	
	@DatabaseField(dataType=DataType.STRING, columnName=FIELD_DEVICE_CATEGORY_NAME)
	private String device_category_name; 
	
	
	public DeviceCategory(){
		
	}
	
	public DeviceCategory(long id, String name){
		this.device_category_name = name;
		
		device_category_id = id;
	}
	
	public String getName(){
		
		return this.device_category_name;
	}
	
	@Override
	public String toString(){
	return getName();
	}
	
	
	
	

}
