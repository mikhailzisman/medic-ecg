package com.medic.DAO;

import java.sql.SQLException;
import java.util.List;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;
import com.medic.DAO.Medic;
import com.medic.fftshow.R;

public class MedicDAO extends BaseDaoImpl<Medic, Integer> {

	public MedicDAO(ConnectionSource connectionSource, Class<Medic> dataclass)
			throws SQLException {
		super(connectionSource, dataclass);
	}

	private String Login(String login, String password) throws SQLException {

		if (isVerified(login, password))
			return getTokenByLoginPasswordPair(login, password);
		else
			return null;

	}

	private String getTokenByLoginPasswordPair(String login, String password) {
		// TODO Auto-generated method stub
		return null;
	}

	public boolean isVerified(String login, String password)
			throws SQLException {
		return (getByLoginAndPasswordPair(login, password).size() > 0);
	}

	public List<Medic> getAll() throws SQLException {
		return this.queryForAll();
	}

	public List<Medic> getByLoginAndPasswordPair(String login, String password)
			throws SQLException {
		QueryBuilder<Medic, Integer> queryBuilder = queryBuilder();
		queryBuilder.where().eq(Medic.FIELD_LOGIN_NAME, login).and()
				.eq(Medic.FIELD_PASSWORD_NAME, password);
		PreparedQuery<Medic> preparedQuery = queryBuilder.prepare();
		List<Medic> medicList = query(preparedQuery);
		return medicList;
	}

	public void addMedic(Medic medic) throws SQLException {
		this.create(medic);
	}
	
	public void deleteMedic(Medic medic) throws SQLException{
		this.delete(medic);
	}
	
	public void updateMedic(Medic medic) throws SQLException{
		this.update(medic);
	}
		

}