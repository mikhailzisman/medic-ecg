package com.medic.DAO;

import java.sql.SQLException;
import java.util.List;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;
import com.medic.fftshow.R;

public class DeviceDAO extends BaseDaoImpl<Device, Integer>{

	public DeviceDAO(ConnectionSource connectionSource, Class<Device> dataclass)
			throws SQLException {
		super(connectionSource, dataclass);
	}
	
	public Device getDeviceById(long id) throws SQLException {
		QueryBuilder<Device, Integer> queryBuilder = queryBuilder();
		queryBuilder.where().eq(Device.FIELD_DEVICE_ID, id);
		PreparedQuery<Device> preparedQuery = queryBuilder.prepare();
		List<Device> deviceList = query(preparedQuery);
		return deviceList.get(0);
	}
	
	public List<Device> getDeviceListByCategoryId(int id) throws SQLException{
		QueryBuilder<Device, Integer> queryBuilder = queryBuilder();
		queryBuilder.where().eq(Device.FIELD_DEVICE_CATEGORY_ID, id);
		PreparedQuery<Device> preparedQuery = queryBuilder.prepare();
		return query(preparedQuery);
	}
	
	public void addDevice(Device device) throws SQLException{
		this.createIfNotExists(device);
	}
	
	public void deleteDevice(Device device) throws SQLException{
		this.delete(device);
	}
	
	public void updateDevice(Device device) throws SQLException{
		this.update(device);
	}

}
