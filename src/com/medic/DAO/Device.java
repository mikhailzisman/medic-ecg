package com.medic.DAO;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.medic.fftshow.R;

@DatabaseTable(tableName = "device")
public class Device {

	public static final String FIELD_DEVICE_NAME = "device_name";
	
	public static final String FIELD_DEVICE_DESCRIPTION = "device_description";

	public static final String FIELD_DEVICE_ID = "device_id";
	
	public static final String FIELD_DEVICE_CATEGORY_ID = "device_category";
	
	@DatabaseField(id=true, dataType=DataType.LONG, columnName=FIELD_DEVICE_ID)
	private long device_id;
	
	@DatabaseField(dataType=DataType.STRING, columnName=FIELD_DEVICE_NAME)
	private String device_model_name;
	
	@DatabaseField(dataType=DataType.STRING, columnName=FIELD_DEVICE_DESCRIPTION)
	private String device_description; 
	
	@DatabaseField(foreign = true, canBeNull = false, columnName = FIELD_DEVICE_CATEGORY_ID)
	private DeviceCategory device_category; 
	
	public Device() {
		// TODO Auto-generated constructor stub
	}
	
	public String getName(){
		return this.device_model_name;
	}
	
	public Device(long id, DeviceCategory device_category, String device_model_name, String device_description){
		
		this.device_category = device_category;
		
		this.device_model_name = device_model_name;
		
		this.device_description = device_description;
		
		this.device_id = id;
	}
	
	@Override
	public String toString(){
	return getName();
	}

}
