package com.medic.DAO.updates;

import java.sql.SQLException;

import com.medic.DAO.Device;
import com.medic.DAO.DeviceCategory;
import com.medic.database.HelperFactory;

public class DeviceAndCategoryUpdater {
	
	static DeviceCategory[] categoryList = { new DeviceCategory(0,"���"), 		 //1
									  		 new DeviceCategory(1,"��������������")//2
										   };	
			

	static Device[] deviceList = { new Device(0, categoryList[0], "������ ������", "�������� 1 �������"), 
						 		   new Device(1,categoryList[0], "������ ������", "�������� 2 �������"),
						 		   new Device(2,categoryList[1], "������ ������", "�������� 3 �������"),
						 		   new Device(3,categoryList[1], "��������� ������", "�������� 4 �������")};

	public static void startUpdateCategotyAndDeviceList() throws SQLException {

		for (DeviceCategory category:categoryList)
			HelperFactory.getHelper().getDeviceCategoryDAO().addCategory(category);
			
		for (Device device : deviceList)
			HelperFactory.getHelper().getDeviceDAO().addDevice(device);
	}
	
}
