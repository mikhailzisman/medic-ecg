package com.medic.DAO;

import java.sql.SQLException;
import java.util.List;


import com.medic.DAO.Patient;
import com.medic.fftshow.R;
import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;

public class PatientDAO extends BaseDaoImpl<Patient, Integer>{

   public PatientDAO(ConnectionSource connectionSource,
           Class<Patient> dataClass) throws SQLException{
       super(connectionSource, dataClass);
   }

   public List<Patient> getAllRoles() throws SQLException{
       return this.queryForAll();
   }


   public void addPatient(Patient pacient) throws SQLException {
		this.create(pacient);
	}
	
	public void deletePatient(Patient pacient) throws SQLException{
		this.delete(pacient);
	}
	
	public void updatePatient(Patient pacient) throws SQLException{
		this.update(pacient);
	}
}