package com.medic.DAO;

import java.sql.Date;
import java.sql.SQLException;
import java.util.List;


import com.medic.DAO.Patient;
import com.medic.database.HelperFactory;
import com.medic.fftshow.R;
import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;

public class TestDAO extends BaseDaoImpl<Test, Integer>{

   public TestDAO(ConnectionSource connectionSource,
           Class<Test> dataClass) throws SQLException{
       super(connectionSource, dataClass);
   }

   public List<Test> getAllRoles() throws SQLException{
       return this.queryForAll();
   }
   
   public void addTest(Medic medic, Patient patient, Device device) throws SQLException{	   
	   Test test = new Test(medic,patient,device);
	   HelperFactory.getHelper().getTestDAO().create(test);
   }
   
   public List<Test> getTestListByPacientID(long pacient_id) throws SQLException{
	   QueryBuilder<Test, Integer> queryBuilder = queryBuilder();
	   queryBuilder.where().eq(Test.FIELD_PACIENT_ID, pacient_id);
	   PreparedQuery<Test> preparedQuery = queryBuilder.prepare();
	   List<Test> testList = query(preparedQuery);
	   return testList;
   }
   
   public List<Test> getTestListByDateBetween(Date date_begin, Date date_end) throws SQLException{
	   QueryBuilder<Test, Integer> queryBuilder = queryBuilder();
	   queryBuilder.where().between(Test.FIELD_DATE, date_begin, date_end);
	   PreparedQuery<Test> preparedQuery = queryBuilder.prepare();
	   List<Test> testList = query(preparedQuery);
	   return testList;
   }
   
   public List<Test> getTestListByMedicID(long medic_id) throws SQLException{
	   QueryBuilder<Test, Integer> queryBuilder = queryBuilder();
	   queryBuilder.where().eq(Test.FIELD_MEDIC_ID, medic_id);
	   PreparedQuery<Test> preparedQuery = queryBuilder.prepare();
	   List<Test> testList = query(preparedQuery);
	   return testList;
   }

   public void addTest(Test test) throws SQLException{
	   this.create(test);
   }
   
   public void deleteTest(Test test) throws SQLException{
	   this.delete(test);
   }
   
   public void updateTest(Test test) throws SQLException{
	   this.update(test);
   }
   
}