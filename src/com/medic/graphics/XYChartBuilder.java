package com.medic.graphics;

import java.util.Timer;
import java.util.TimerTask;
import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.PointStyle;
import org.achartengine.model.Point;
import org.achartengine.model.SeriesSelection;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;
import org.achartengine.tools.PanListener;
import org.achartengine.tools.ZoomEvent;
import org.achartengine.tools.ZoomListener;


import com.medic.fftshow.R;
import com.medic.Audio.RecordAudio;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

public class XYChartBuilder extends Activity {

	public static final String TYPE = "type";
	
	public static boolean RecordRunning = false;

	private XYMultipleSeriesDataset mDataset = new XYMultipleSeriesDataset();

	private XYMultipleSeriesRenderer mRenderer = new XYMultipleSeriesRenderer();

	private static XYSeries mCurrentSeries;

	private XYSeriesRenderer mCurrentRenderer;

	private String mDateFormat;

	private Button mPreviousButton;

	private Button mClearButton;
	
	private Button mNextButton;
	
	private Button mStopButton;

	private static GraphicalView mChartView;

	private double time_on_screen_in_sec = 10;

	protected boolean zoomed = false;
	
	private AsyncTask<Void, short[], Void> rec;

	private Button mStartButton;
	
	private Timer timer = new Timer();
	
	private Activity activity;

	
	@Override
	protected void onRestoreInstanceState(Bundle savedState) {
		super.onRestoreInstanceState(savedState);
		mDataset = (XYMultipleSeriesDataset) savedState
				.getSerializable("dataset");
		mRenderer = (XYMultipleSeriesRenderer) savedState
				.getSerializable("renderer");
		mCurrentSeries = (XYSeries) savedState
				.getSerializable("current_series");
		mCurrentRenderer = (XYSeriesRenderer) savedState
				.getSerializable("current_renderer");
		mDateFormat = savedState.getString("date_format");
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putSerializable("dataset", mDataset);
		outState.putSerializable("renderer", mRenderer);
		outState.putSerializable("current_series", mCurrentSeries);
		outState.putSerializable("current_renderer", mCurrentRenderer);
		outState.putString("date_format", mDateFormat);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.xy_chart);
		activity=this;
		mRenderer.setApplyBackgroundColor(true);
		mRenderer.setBackgroundColor(Color.argb(100, 50, 50, 50));
		mRenderer.setAxisTitleTextSize(16);
		mRenderer.setChartTitleTextSize(20);
		mRenderer.setLabelsTextSize(15);
		mRenderer.setLegendTextSize(15);
		mRenderer.setMargins(new int[] { 20, 30, 15, 0 });
		mRenderer.setZoomButtonsVisible(true);
		mRenderer.setPointSize(0);

		mClearButton = (Button) findViewById(R.id.clear);
		
		mStartButton = (Button) findViewById(R.id.start);
		mStartButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				AddNewSeries();
				
				RecordRunning = true;

				rec = new RecordAudio();	

				rec.execute();
				
				timer.scheduleAtFixedRate(new TimerTask() {

					@Override
					public void run() {
						
						if (!zoomed ){
							mRenderer.setXAxisMin(mCurrentSeries.getMaxX()-time_on_screen_in_sec);
							mRenderer.setXAxisMax(mCurrentSeries.getMaxX());
						}
					
						RepaintChart();

					}
				}, 0, 300);
				
				setEnabledButton(false);
				
				mStopButton.setEnabled(true);
			}
		});
		
		mStopButton = (Button) findViewById(R.id.stop);
		mStopButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				RecordRunning = false;
				
				setEnabledButton(true);
				
				mStopButton.setEnabled(false);
				
			}
		});
		
		mNextButton = (Button)findViewById(R.id.next);
		mNextButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				Intent segmentsActivity = new Intent(activity,SegmentsActivity.class);
				
				segmentsActivity.putExtra("points", mCurrentSeries);
				
				startActivity(segmentsActivity);
			}
		});
		
		
		mPreviousButton = (Button) findViewById(R.id.previous_button);
		mPreviousButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				onBackPressed();
			}
		});

		

		

		

		mClearButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				
				synchronized (mCurrentSeries) {
					mCurrentSeries.clear();
				}

			}

		});
	}

	private void AddNewSeries() {
		String seriesTitle = "Series " + (mDataset.getSeriesCount() + 1);
		XYSeries series = new XYSeries(seriesTitle);
		mDataset.addSeries(series);
		mCurrentSeries = series;
		XYSeriesRenderer renderer = new XYSeriesRenderer();
		mRenderer.addSeriesRenderer(renderer);
		renderer.setPointStyle(PointStyle.CIRCLE);
		renderer.setFillPoints(true);
		mCurrentRenderer = renderer;
		setSeriesEnabled(true);
	}

	public static void AddNewPoint(double x, double y) {

		mCurrentSeries.add(x, y);
		
		/*if InStrobeRange(y){			
			TimesInStrobe ++;
			if (InTimeRange(TimesInStrobe*)){
				TimesInStrobe = 0;				
				strobeList = x;						
			}												
		}else{
			TimesInStrobe = 0;
		}*/
		
		/*if (WasStrobe) {
			if ((x-begin_time)<2.5){
				addSeries.add(x,y);
			} else{	
			WasStrobe = false;
			ArraList<XYSeries> list.add(addSeries);
			}
		}*/
	}

	


	private static void RepaintChart() {

		if (mChartView != null) {
			mChartView.repaint();
		}

	}

	@Override
	protected void onResume() {
		super.onResume();
		if (mChartView == null) {
			LinearLayout layout = (LinearLayout) findViewById(R.id.chart);
			mChartView = ChartFactory.getLineChartView(this, mDataset,
					mRenderer);
			mRenderer.setClickEnabled(true);
			mRenderer.setSelectableBuffer(100);
			mChartView.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					SeriesSelection seriesSelection = mChartView
							.getCurrentSeriesAndPoint();
					double[] xy = mChartView.toRealPoint(0);
					if (seriesSelection == null) {
						Toast.makeText(XYChartBuilder.this,
								"No chart element was clicked",
								Toast.LENGTH_SHORT).show();
					} else {
						Toast.makeText(
								XYChartBuilder.this,
								"Chart element in series index "
										+ seriesSelection.getSeriesIndex()
										+ " data point index "
										+ seriesSelection.getPointIndex()
										+ " was clicked"
										+ " closest point value X="
										+ seriesSelection.getXValue() + ", Y="
										+ seriesSelection.getValue()
										+ " clicked point value X="
										+ (float) xy[0] + ", Y="
										+ (float) xy[1], Toast.LENGTH_SHORT)
								.show();
					}
				}
			});
			mChartView.setOnLongClickListener(new View.OnLongClickListener() {
				@Override
				public boolean onLongClick(View v) {
					SeriesSelection seriesSelection = mChartView
							.getCurrentSeriesAndPoint();
					if (seriesSelection == null) {
						Toast.makeText(XYChartBuilder.this,
								"No chart element was long pressed",
								Toast.LENGTH_SHORT);
						return false; // no chart element was long pressed, so
										// let something
						// else handle the event
					} else {
						Toast.makeText(XYChartBuilder.this,
								"Chart element in series index "
										+ seriesSelection.getSeriesIndex()
										+ " data point index "
										+ seriesSelection.getPointIndex()
										+ " was long pressed",
								Toast.LENGTH_SHORT);
						return true; // the element was long pressed - the event
										// has been
						// handled
					}
				}
			});
			mChartView.addZoomListener(new ZoomListener() {
				

				public void zoomApplied(ZoomEvent e) {
					String type = "out";
					if (e.isZoomIn()) {
						type = "in";
					}
					System.out.println("Zoom " + type + " rate "
							+ e.getZoomRate());
					
					zoomed = true;
				}

				public void zoomReset() {
					System.out.println("Reset");
					
					zoomed = false;
				}
			}, true, true);
			mChartView.addPanListener(new PanListener() {
				public void panApplied() {
					System.out.println("New X range=["
							+ mRenderer.getXAxisMin() + ", "
							+ mRenderer.getXAxisMax() + "], Y range=["
							+ mRenderer.getYAxisMax() + ", "
							+ mRenderer.getYAxisMax() + "]");
				}

			});
			layout.addView(mChartView, new LayoutParams(
					LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
			boolean enabled = mDataset.getSeriesCount() > 0;
			setSeriesEnabled(enabled);
		} else {
			mChartView.repaint();
		}
	}

	private void setSeriesEnabled(boolean enabled) {		
		mClearButton.setEnabled(enabled);
	}
	
	private void setEnabledButton (boolean mode){
		
		mNextButton.setEnabled(mode);
		
		mPreviousButton.setEnabled(mode);
		
		mStartButton.setEnabled(mode);
		
		mStopButton.setEnabled(mode);
		
		mClearButton.setEnabled(mode);
	}
	
	
	

}
