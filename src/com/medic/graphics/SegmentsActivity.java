package com.medic.graphics;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.PointStyle;
import org.achartengine.model.Point;
import org.achartengine.model.SeriesSelection;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;
import org.achartengine.tools.PanListener;
import org.achartengine.tools.ZoomEvent;
import org.achartengine.tools.ZoomListener;

import com.medic.demodulator.deviderIntoSegments;
import com.medic.fftshow.R;
import com.medic.Audio.RecordAudio;

import android.app.Activity;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.ToggleButton;

public class SegmentsActivity extends Activity {

	public static final String TYPE = "Segments";

	private XYMultipleSeriesDataset mDataset = new XYMultipleSeriesDataset();

	private XYMultipleSeriesRenderer mRenderer = new XYMultipleSeriesRenderer();

	private static XYSeries mCurrentSeries;

	private XYSeriesRenderer mCurrentRenderer;

	private String mDateFormat;

	private ArrayList<XYSeries> devidedSeries;

	List<ToggleButton> arrayListToggleButtons;

	private deviderIntoSegments devider = new deviderIntoSegments();

	private static GraphicalView mChartView;

	protected boolean zoomed = false;

	private Activity activity;

	@Override
	protected void onRestoreInstanceState(Bundle savedState) {
		super.onRestoreInstanceState(savedState);
		mDataset = (XYMultipleSeriesDataset) savedState
				.getSerializable("dataset");
		mRenderer = (XYMultipleSeriesRenderer) savedState
				.getSerializable("renderer");
		mCurrentSeries = (XYSeries) savedState
				.getSerializable("current_series");
		mCurrentRenderer = (XYSeriesRenderer) savedState
				.getSerializable("current_renderer");
		mDateFormat = savedState.getString("date_format");
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putSerializable("dataset", mDataset);
		outState.putSerializable("renderer", mRenderer);
		outState.putSerializable("current_series", mCurrentSeries);
		outState.putSerializable("current_renderer", mCurrentRenderer);
		outState.putString("date_format", mDateFormat);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.graphs);
/*
		ToggleButton[] graphButtons = {
				(ToggleButton) findViewById(R.id.graph_leadII_button),
				(ToggleButton) findViewById(R.id.graphI_button),
				(ToggleButton) findViewById(R.id.graphII_button),
				(ToggleButton) findViewById(R.id.graphIII_button),
				(ToggleButton) findViewById(R.id.graphaVR_button),
				(ToggleButton) findViewById(R.id.graphaVL_button),
				(ToggleButton) findViewById(R.id.graphaVF_button),
				(ToggleButton) findViewById(R.id.graphV1_button),
				(ToggleButton) findViewById(R.id.graphV2_button),
				(ToggleButton) findViewById(R.id.graphV3_button),
				(ToggleButton) findViewById(R.id.graphV4_button),
				(ToggleButton) findViewById(R.id.graphV5_button),
				(ToggleButton) findViewById(R.id.graphV6_button) };

		arrayListToggleButtons = Arrays.asList(graphButtons);

		Iterator<ToggleButton> iter = arrayListToggleButtons.iterator();
		while (iter.hasNext()) {
			iter.next().setOnClickListener(OnToggleListener);
		}
*/		
		Spinner segmentChooser = (Spinner)findViewById(R.id.segmentChooser);
		List<String> seg_names = new ArrayList<String>();
		seg_names.add("1");
		seg_names.add("2");
		seg_names.add("3");
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, seg_names);
		segmentChooser.setAdapter(adapter);
		segmentChooser.setOnItemSelectedListener(OnSelectedListener);
		mRenderer.setApplyBackgroundColor(true);
		mRenderer.setBackgroundColor(Color.argb(100, 50, 50, 50));
		mRenderer.setAxisTitleTextSize(16);
		mRenderer.setChartTitleTextSize(20);
		mRenderer.setLabelsTextSize(15);
		mRenderer.setLegendTextSize(15);
		mRenderer.setMargins(new int[] { 20, 30, 15, 0 });
		mRenderer.setZoomButtonsVisible(true);
		mRenderer.setPointSize(0);

		mCurrentSeries = (XYSeries) getIntent().getSerializableExtra("points");

		devidedSeries = new ArrayList<XYSeries>();

		devidedSeries = deviderIntoSegments.divide(mCurrentSeries);

		activity = this;
	}

	private void AddNewSeries() {
		String seriesTitle = "Series " + (mDataset.getSeriesCount() + 1);
		XYSeries series = new XYSeries(seriesTitle);
		mDataset.addSeries(series);
		mCurrentSeries = series;
		XYSeriesRenderer renderer = new XYSeriesRenderer();
		mRenderer.addSeriesRenderer(renderer);
		renderer.setPointStyle(PointStyle.CIRCLE);
		renderer.setFillPoints(true);
		mCurrentRenderer = renderer;
	}

	public static void AddNewPoint(double x, double y) {

		mCurrentSeries.add(x, y);
	}

	private static void RepaintChart() {

		if (mChartView != null) {
			mChartView.repaint();
		}

	}

	@Override
	protected void onResume() {
		super.onResume();
		if (mChartView == null) {
			LinearLayout layout = (LinearLayout) findViewById(R.id.chart);
			mChartView = ChartFactory.getLineChartView(this, mDataset,
					mRenderer);
			mRenderer.setClickEnabled(true);
			mRenderer.setSelectableBuffer(100);
			mChartView.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					SeriesSelection seriesSelection = mChartView
							.getCurrentSeriesAndPoint();
					double[] xy = mChartView.toRealPoint(0);
					if (seriesSelection == null) {
						Toast.makeText(SegmentsActivity.this,
								"No chart element was clicked",
								Toast.LENGTH_SHORT).show();
					} else {
						Toast.makeText(
								SegmentsActivity.this,
								"Chart element in series index "
										+ seriesSelection.getSeriesIndex()
										+ " data point index "
										+ seriesSelection.getPointIndex()
										+ " was clicked"
										+ " closest point value X="
										+ seriesSelection.getXValue() + ", Y="
										+ seriesSelection.getValue()
										+ " clicked point value X="
										+ (float) xy[0] + ", Y="
										+ (float) xy[1], Toast.LENGTH_SHORT)
								.show();
					}
				}
			});
			mChartView.setOnLongClickListener(new View.OnLongClickListener() {
				@Override
				public boolean onLongClick(View v) {
					SeriesSelection seriesSelection = mChartView
							.getCurrentSeriesAndPoint();
					if (seriesSelection == null) {
						Toast.makeText(SegmentsActivity.this,
								"No chart element was long pressed",
								Toast.LENGTH_SHORT);
						return false; // no chart element was long pressed, so
										// let something
						// else handle the event
					} else {
						Toast.makeText(SegmentsActivity.this,
								"Chart element in series index "
										+ seriesSelection.getSeriesIndex()
										+ " data point index "
										+ seriesSelection.getPointIndex()
										+ " was long pressed",
								Toast.LENGTH_SHORT);
						return true; // the element was long pressed - the event
										// has been
						// handled
					}
				}
			});
			mChartView.addZoomListener(new ZoomListener() {

				public void zoomApplied(ZoomEvent e) {
					String type = "out";
					if (e.isZoomIn()) {
						type = "in";
					}
					System.out.println("Zoom " + type + " rate "
							+ e.getZoomRate());

					zoomed = true;
				}

				public void zoomReset() {
					System.out.println("Reset");

					zoomed = false;
				}
			}, true, true);
			mChartView.addPanListener(new PanListener() {
				public void panApplied() {
					System.out.println("New X range=["
							+ mRenderer.getXAxisMin() + ", "
							+ mRenderer.getXAxisMax() + "], Y range=["
							+ mRenderer.getYAxisMax() + ", "
							+ mRenderer.getYAxisMax() + "]");
				}

			});
			layout.addView(mChartView, new LayoutParams(
					LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
			boolean enabled = mDataset.getSeriesCount() > 0;

		} else {
			mChartView.repaint();
		}
	}

	AdapterView.OnItemSelectedListener OnSelectedListener = new AdapterView.OnItemSelectedListener() {


		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			
			

				int indexButton = arg2;

				if (devidedSeries.size()>indexButton){
					
					AddNewSeries();
					
					mCurrentSeries = devidedSeries.get(indexButton-1);
					
					Toast.makeText(activity,
							"We have " + devidedSeries.size()
									+ " segments. In this series :"+mCurrentSeries.getItemCount()
									+ "points", 2000).show();
				}

				RepaintChart();
		}
			
		

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub
			
		};
	};

}
