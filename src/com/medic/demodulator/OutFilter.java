package com.medic.demodulator;

public class OutFilter
{
	private double _si_1 = 0.00024395736285062899;
	private double _a2_1 = -1.9951254700780645;
	private double _a3_1 = 0.99610129952946713;
	private double _b2_1 = 2.0;
	private double _so_1 = 1.0;

	private double _si_2 = 0.00013688497949128692;
	private double _a2_2 = -1.9888357263658569;
	private double _a3_2 = 0.989383266283822;
	private double _b2_2 = 2;
	private double _so_2 = 1.0;

	private double _si_3 = 0.000030551561069144025;
	private double _a2_3 = -1.9854016544062538;
	private double _a3_3 = 0.98552386065053021;
	private double _b2_3 = 2.0;
	private double _so_3 = 0.89125093813374556; 

	firstFilter _filterSection1;
	firstFilter _filterSection2;
	firstFilter _filterSection3;

	public OutFilter()
	{
		_filterSection1 = new firstFilter(_si_1, _a2_1, _a3_1, _b2_1, _so_1);
		_filterSection2 = new firstFilter(_si_2, _a2_2, _a3_2, _b2_2, _so_2);
		_filterSection3 = new firstFilter(_si_3, _a2_3, _a3_3, _b2_3, _so_3);
	}

	double filter(double input)
	{
		double outTemp = _filterSection1.filter(input);
		outTemp = _filterSection2.filter(outTemp);
		return _filterSection3.filter(outTemp);
	} 
}