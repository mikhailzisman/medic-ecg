package com.medic.demodulator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.NavigableMap;
import java.util.NavigableSet;
import java.util.Set;
import java.util.SortedMap;

import org.achartengine.model.XYSeries;
import org.achartengine.renderer.XYSeriesRenderer;

public class deviderIntoSegments {

	static ArrayList<XYSeries> listOfDividedSeries;

	// static double step = 1/8000;

	static double TimeMinWidthStrobe = 0.05;

	static double TimeMaxWidthStrobe = 0.1;

	static double MaxHeightStrobe = 0.065;//0.6

	static double MinHeightStrobe = 0.05;//0.55

	public deviderIntoSegments() {
		// TODO Auto-generated constructor stub
	}

	static public ArrayList<XYSeries> divide(XYSeries source) {

		listOfDividedSeries = new ArrayList<XYSeries>();

		double prev_begin_strobe = 0;

		double begin_Strobe_time = 0;

		double end_Strobe_time = 0;

		double diff_time = 0;

		double time = 0;

		double value = 0;
		
		double StrobeTimer = 0;

		if (source == null)
			return listOfDividedSeries;

		for (int step = 0; step < source.getItemCount(); step++) {

			time = source.getX(step);

			if (step >= 1) {
				diff_time = source.getX(step) - source.getX(step - 1);
			}

			value = source.getY(step);

			if (!inStrobeRange(value)) {

				if (StrobeTimer != 0) {

					end_Strobe_time = time;

					if (inStrobeTimeBounds(begin_Strobe_time, end_Strobe_time)) {

						listOfDividedSeries
								.add(convertSortedMapToXYSeries(source
										.getRange(prev_begin_strobe,
												begin_Strobe_time, 0)));
						prev_begin_strobe = begin_Strobe_time;
					}

				}

				StrobeTimer = 0;
			} else {
				if (StrobeTimer == 0) {

					begin_Strobe_time = time;

				}
				StrobeTimer += diff_time;
			}

		}

		return listOfDividedSeries;
	}

	private static XYSeries convertSortedMapToXYSeries(
			SortedMap<Double, Double> range) {

		Set<Entry<Double, Double>> map = range.entrySet();

		Iterator<Entry<Double, Double>> iter = map.iterator();

		XYSeries curSeries = new XYSeries("");

		while (iter.hasNext()) {

			Entry<Double, Double> val = iter.next();

			curSeries.add(val.getKey(), val.getValue());
		}

		return curSeries;
	}

	private static boolean inStrobeTimeBounds(double begin, double end) {
		return ((end - begin) < TimeMaxWidthStrobe)
				&& ((end - begin) > TimeMinWidthStrobe);
	}

	private static boolean inStrobeRange(double value) {
		return (value > MinHeightStrobe) && (value < MaxHeightStrobe);
	}

}
