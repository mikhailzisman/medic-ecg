package com.medic.demodulator;

import java.lang.Math;

public class Decoder
{
	final double Fs = 8000;
	final double Fc = 1900;
	double carrierPhase = 0;
	static firstFilter _filterRe;
	static firstFilter _filterIm;
	double prevPhase = 0;
	double pi = Math.PI;
	OutFilter OutFilter;
	
	public Decoder()
	{
		_filterRe = new firstFilter();
		_filterIm = new firstFilter();
		OutFilter = new OutFilter();
	}

	public double decode(double input)
	{
		carrierPhase = carrierPhase + 2*pi*Fc/Fs;
		double inRe = input * Math.cos(carrierPhase);
		double inIm = input * Math.sin(carrierPhase);
		double fltRe = _filterRe.filter(inRe);//, firstFilterRegsRe);
		double fltIm = _filterIm.filter(inIm);//, firstFilterRegsIm);
		double currentPhase;
		if (fltRe != 0)
			currentPhase = Math.atan(fltIm/fltRe);
		else
			if (fltIm > 0)
				currentPhase = pi/2;
			else 
				currentPhase = -pi/2;
            
		double delta = currentPhase - prevPhase;
		if (delta < -pi/2)
			delta = pi + delta;
		else
			if (delta > pi/2)
				delta = -(pi - delta);
		prevPhase = currentPhase;
		return OutFilter.filter(delta);
	}
}