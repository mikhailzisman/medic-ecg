package com.medic.demodulator;

class firstFilter
{
	private double[] _regs = new double[2] ;
	//22050 Hz
	/*
	private double _si = 0.001345437858596008;
	private double _a2 = -1.9194311696923929;
	private double _a3 = 0.92481292112677682;
	private double _b2 = 2;
	private double _so = 0.89125093813374556;
	*/
	
	// 11025 Hz
	/*
	private double _si = 0.0051792667053313351;
	private double _a2 = -1.8347502592889544;
	private double _a3 = 0.85546732611027954;
	private double _b2 = 2;
	private double _so = 0.89125093813374556;
	*/
	//8000 Hz
	private double _si = 0.009559211326762445;
	private double _a2 = -1.7684914344954319;
	private double _a3 = 0.80672827980248174;
	private double _b2 = 2;
	private double _so = 0.89125093813374556;
	
	
	
	firstFilter(){
		_regs[0] = 0;
		_regs[1] = 0;				
	}
	
	firstFilter(double si, double a2,  double a3, double b2, double so)
	{
		super();
		_si = si;
		_a2 = a2;
		_a3 = a3;
		_b2 = b2;
		_so = so;
	}

	double filter(double input)
	{
		
		double denomCalc = input * _si - _regs[0] * _a2 - _regs[1] * _a3;
		double result = (denomCalc + _b2 * _regs[0] + _regs[1]) * _so;
		_regs[1] = _regs[0];
		_regs[0] = denomCalc;
		return result;
	}
}
