package com.medic.activities;

import java.sql.SQLException;

import com.medic.DAO.Device;
import com.medic.DAO.Medic;
import com.medic.DAO.Patient;
import com.medic.DAO.Test;
import com.medic.database.HelperFactory;
import com.medic.fftshow.R;
import com.medic.graphics.XYChartBuilder;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class TestActivity extends Activity {

	private static final String TAG = "TestActivity";
	Test mCurTest;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.test_start);

		Button startButton = (Button) findViewById(R.id.startTest);

		startButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				mCurTest = createNewTest(Login.mCurrentMedic,
						PatientRegistration.mCurPacient,
						DeviceChooser.mCurDeviceModel);

				startTest();

			}
		});

	}

	private void startTest() {

		Intent intent = new Intent(this, XYChartBuilder.class);

		startActivity(intent);

	}

	private Test createNewTest(Medic mCurrentMedic, Patient mCurPacient,
			Device mCurDeviceModel) {

		Test test = new Test(mCurrentMedic, mCurPacient, mCurDeviceModel);

		try {

			HelperFactory.getHelper().getTestDAO().addTest(test);

		} catch (SQLException e) {
			Log.e(TAG, "Error database - add test");
			e.printStackTrace();
		}

		return test;
	}

}
