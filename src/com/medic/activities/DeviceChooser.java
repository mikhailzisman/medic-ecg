package com.medic.activities;

import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;

import com.medic.DAO.Device;
import com.medic.DAO.DeviceCategory;
import com.medic.DAO.updates.DeviceAndCategoryUpdater;
import com.medic.adapters.DeviceAdapter;
import com.medic.adapters.DeviceCategoryAdapter;
import com.medic.database.HelperFactory;
import com.medic.fftshow.R;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

public class DeviceChooser extends Activity {
	
	
	
	private static final String TAG = "Device chooser activity";

	Spinner device_model;
	
	Spinner device_category;
	
	Activity activity;

	private AdapterView.OnItemSelectedListener OnCategorySelectedListener = new OnItemSelectedListener() {

		@Override
		public void onItemSelected(AdapterView<?> items, View view, int index,
				long arg3) {
			
			try {
								
				
				DeviceAdapter device_model_adapter = new DeviceAdapter(activity, android.R.layout.simple_spinner_item, 
							HelperFactory.getHelper().getDeviceDAO().getDeviceListByCategoryId(index));
				
				device_model_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
				
				device_model.setAdapter(device_model_adapter);	
				
				mCurDeviceCategory = (DeviceCategory) items.getItemAtPosition(index);
				
				
			} catch (SQLException e) {
				Log.e(TAG, "Error read models");
				e.printStackTrace();
			}
			
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub
			
		}
	};

	private AdapterView.OnItemSelectedListener OnModelSelectedListener = new OnItemSelectedListener() {


		@Override
		public void onItemSelected(AdapterView<?> items, View view, int index,
				long arg3) {
			
			Device mCurDevice= (Device)items.getItemAtPosition(index);	
			
			
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub
			
		}
	};

	private TextView doctor_name;

	private TextView pacient_name;

	private TextView device_name;

	private TextView device_info;
	
	public static DeviceCategory mCurDeviceCategory;
	
	public static Device mCurDeviceModel;

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		
		activity = this;
		
		setContentView(R.layout.device_choice);
		
		device_category = (Spinner) findViewById(R.id.device_category);
		
		device_category.setOnItemSelectedListener(OnCategorySelectedListener );
		
		device_model = (Spinner) findViewById(R.id.device_model);
		
		device_model.setOnItemSelectedListener(OnModelSelectedListener );
		
		doctor_name = (TextView) findViewById(R.id.doctor_name);
		
		pacient_name = (TextView) findViewById(R.id.patient_name);
		
		device_name = (TextView) findViewById(R.id.device_name);
		
		device_info = (TextView) findViewById(R.id.device_info);
		

		try {
			
			DeviceAndCategoryUpdater.startUpdateCategotyAndDeviceList();
			
			setDeviceCategorySpinner();
		} catch (SQLException e) {

			Log.e(TAG, "Error device and category list updater");
			
			e.printStackTrace();
		}
		
		
				
	
	}


	private void setDeviceCategorySpinner() {
		DeviceCategoryAdapter device_category_adapter;
		try {
			device_category_adapter = new DeviceCategoryAdapter(this, android.R.layout.simple_spinner_item, 
						HelperFactory.getHelper().getDeviceCategoryDAO().getAllDevicesCategories());
			
			device_category_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			
			device_category.setAdapter(device_category_adapter);
			
			
		} catch (SQLException e) {
			Log.e(TAG, "Error read categories from DB");
			e.printStackTrace();
		}
	}	

}
