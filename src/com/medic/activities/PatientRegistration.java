package com.medic.activities;

import java.sql.SQLException;

import com.medic.DAO.Patient;
import com.medic.database.HelperFactory;
import com.medic.fftshow.R;
import com.medic.utils.Validator;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class PatientRegistration extends Activity {
	
	private String TAG = "Patient registration";

	Spinner genderSpinner;

	Button nextButton;

	EditText patientIdEditText;

	EditText patientNameEditText;
	
	EditText patientSurnameEditText;

	EditText patientAdditionalInfoEditText;

	EditText patientMysterio;

	Spinner patientGender;
	
	public static Patient mCurPacient;
	

	public PatientRegistration() {
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.patient_registration);

		if (Login.mCurrentMedic == null)
			this.onBackPressed();

		initUI();

	}

	private void initUI() {

		setContentView(R.layout.patient_registration);

		genderSpinner = (Spinner) findViewById(R.id.registration_gender);

		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
				this, R.array.genders, android.R.layout.simple_spinner_item);

		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		genderSpinner.setAdapter(adapter);

		nextButton = (Button) findViewById(R.id.next_button);

		nextButton.setOnClickListener(nextButtonOnClickListener);
		
		patientIdEditText = (EditText) findViewById(R.id.patient_registration_id);
		
		patientNameEditText = (EditText) findViewById(R.id.patient_registration_name);
		
		patientSurnameEditText = (EditText) findViewById(R.id.patient_registration_surname);
		
		patientAdditionalInfoEditText = (EditText) findViewById(R.id.patient_registration_additional_info);
		
		patientGender = (Spinner) findViewById(R.id.registration_gender);

	}

	private View.OnClickListener nextButtonOnClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			

			String patientIdentInfo = patientIdEditText.getText().toString();
			
			String patientName = patientNameEditText.getText().toString();
			
			String patientSurname = patientSurnameEditText.getText().toString();
			
			String patientAdditionalInfo = patientAdditionalInfoEditText.getText().toString();
			
			boolean gender = (patientGender.getSelectedItemPosition() == 0) ? true : false;
			
			if (isValide(patientIdentInfo, patientName, patientSurname)){
				
				mCurPacient = new Patient(patientIdentInfo, patientName, patientSurname, gender, patientAdditionalInfo);
				
				try {
					
					HelperFactory.getHelper().getPatientDAO().addPatient(mCurPacient);
					
					goDeviceChooserActivity();
					
				} catch (SQLException e) {
					
					Log.e(TAG, "Database - add pacient error");
					
					e.printStackTrace();
				}
			} else{

				Toast.makeText(getApplicationContext(), R.string.error_input_pacient_info, 3000).show();
				 
			}
		}

		private boolean isValide(String patientIdentInfo, String patientName, String patientSurname) {
			
			return Validator.isValidInput(patientIdentInfo)&&
				   Validator.isValidInput(patientName)&&
				   Validator.isValidInput(patientSurname);
		}
	};
	
	private void goDeviceChooserActivity(){
		Intent goNext = new Intent(this, DeviceChooser.class);        
		startActivity(goNext);

	}

}
