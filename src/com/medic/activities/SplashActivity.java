package com.medic.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.medic.fftshow.R;

public class SplashActivity extends Activity{
	
	Context context;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		context = this.getApplication();
		
		Intent loginIntent = new Intent(this, Login.class);
		
		loginIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		
		context.startActivity(loginIntent);
		
		
	}

	public SplashActivity() {
		// TODO Auto-generated constructor stub
	}

}
