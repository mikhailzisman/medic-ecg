package com.medic.activities;

import java.sql.SQLException;
import java.util.List;

import com.medic.DAO.Medic;
import com.medic.DAO.MedicDAO;
import com.medic.database.HelperFactory;
import com.medic.fftshow.R;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Login extends Activity {

	String TAG = "Login activity";

	Button enterButton;

	EditText loginTextBox;

	EditText passwordTextBox;

	EditText inputName;

	EditText inputSurname;

	Context context;

	private Login activity;

	static Medic mCurrentMedic;

	static boolean logined = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login);

		enterButton = (Button) findViewById(R.id.enter_button);

		loginTextBox = (EditText) findViewById(R.id.login_text_box);

		passwordTextBox = (EditText) findViewById(R.id.password_text_box);

		enterButton.setOnClickListener(OnEnterClickListener);

		context = getApplicationContext();

		activity = this;

	}

	private android.view.View.OnClickListener OnEnterClickListener = new View.OnClickListener() {

		@Override
		public void onClick(View v) {

			if (valideLoginPasswordInput()) {

				String login = getLoginFromEditBox();

				String password = getPasswordFromEditBox();

				MedicDAO medicDAO;

				try {
					medicDAO = HelperFactory.getHelper().getMedicDAO();

					if (medicDAO.isVerified(login, password)) {

						List<Medic> medicList = medicDAO
								.getByLoginAndPasswordPair(login, password);

						if (medicList.size() > 1) {

							Toast.makeText(context,
									R.string.Error_duplicate_login_password,
									2000).show();

						} else if (medicList.size() == 1) {

							login(medicList.get(0));
														

						}

					} else {

						showRegisterDialog();
					}

				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			} else {

				Toast.makeText(context, R.string.input_not_correct, 2000)
						.show();
			}

		}

	};

	private void showRegisterDialog() {

		AlertDialog.Builder builder = new AlertDialog.Builder(activity);
		builder.setMessage(R.string.login_not_defined)
				.setPositiveButton(R.string.yes, dialogClickListener)
				.setNegativeButton(R.string.no, dialogClickListener).show();

	}

	private void showEnterNameDialog() {
		// This example shows how to add a custom layout to an AlertDialog
		LayoutInflater factory = LayoutInflater.from(this);
		final View textEntryView = factory.inflate(
				R.layout.alert_dialog_text_entry, null);

		inputName = (EditText) textEntryView
				.findViewById(R.id.inputNameRegistration);

		inputSurname = (EditText) textEntryView
				.findViewById(R.id.inputSurnameRegistration);

		AlertDialog.Builder builder = new AlertDialog.Builder(activity);
		builder.setMessage(R.string.inputName)
				.setView(textEntryView)
				.setPositiveButton(R.string.readyInputInformation,
						dialogReadyInputInformationClickListener).show();

	}

	DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
		@Override
		public void onClick(DialogInterface dialog, int which) {

			switch (which) {

			case DialogInterface.BUTTON_POSITIVE:

				showEnterNameDialog();

				break;

			case DialogInterface.BUTTON_NEGATIVE:
				// No button clicked
				break;
			}

			dialog.dismiss();
		}
	};

	DialogInterface.OnClickListener dialogReadyInputInformationClickListener = new DialogInterface.OnClickListener() {
		@Override
		public void onClick(DialogInterface dialog, int which) {

			switch (which) {

			case DialogInterface.BUTTON_POSITIVE:

				if (!valideNameSurnameInput()) {
					dialog.dismiss();
				}
				// Yes button clicked
				try {
					HelperFactory
							.getHelper()
							.getMedicDAO()
							.addMedic(
									new Medic(getLoginFromEditBox(),
											  getPasswordFromEditBox(),
											  getNameFromEditBox(),
											  getSurnameFromEditBox()));
					
					login(HelperFactory.getHelper()
							.getMedicDAO()
							.getByLoginAndPasswordPair(getLoginFromEditBox(), 
									getPasswordFromEditBox()).
									get(0));
					
				} catch (SQLException e) {

					Log.e(TAG, "Adding account error");
					e.printStackTrace();

				}

				break;
			// dialog.dismiss();
			}
		};
	};

	private String getLoginFromEditBox() {
		return loginTextBox.getText().toString().trim();
	}

	private String getPasswordFromEditBox() {
		return passwordTextBox.getText().toString().trim();
	}

	private String getNameFromEditBox() {
		return inputName.getText().toString().trim();
	}

	private String getSurnameFromEditBox() {
		return inputSurname.getText().toString().trim();
	}

	private boolean valideLoginPasswordInput() {
		return !getLoginFromEditBox().equals("")
				&& !getPasswordFromEditBox().equals("");
	}

	private boolean valideNameSurnameInput() {
		return !getLoginFromEditBox().equals("")
				&& !getPasswordFromEditBox().equals("");
	}

	public void login(Medic medic){
		
		if (medic == null) return;
		
		mCurrentMedic = medic;

		logined = true;
		
		goPacientActivity();
	}
	
	public void logout() {

		mCurrentMedic = null;

		logined = false;
	}
	
	private void goPacientActivity(){
		Intent goNext = new Intent(this, PatientRegistration.class);        
		startActivity(goNext);

	}

	@Override
	public void onBackPressed() {
		// Блокируем
	}

}
