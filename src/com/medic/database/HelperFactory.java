package com.medic.database;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.medic.fftshow.R;

import android.content.Context;

public class HelperFactory {
	
	private static DatabaseHelper databaseHelper;
	
	public static DatabaseHelper getHelper(){
		return databaseHelper;
	}
	
	public static void SetHelper(Context context){
	       databaseHelper = OpenHelperManager.getHelper(context, DatabaseHelper.class);
	}
	
	public static void ReleaseHelper(){
	       OpenHelperManager.releaseHelper();
	       databaseHelper = null;
	   }
}
