package com.medic.adapters;

import java.util.List;

import com.medic.DAO.Device;
import com.medic.DAO.DeviceCategory;
import com.medic.fftshow.R;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class DeviceAdapter extends ArrayAdapter<Device> {

	Activity activity;
	private List<Device> listDevices; 
	
	public DeviceAdapter(Activity activity, int textViewResourceId,
			List<Device> objects) {
		
		super(activity, textViewResourceId, objects);
		
		this.activity = activity;
		
		this.listDevices = objects;
		
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		View rowView = convertView;
		
		if (rowView ==null){
			
			LayoutInflater inflater = activity.getLayoutInflater();
			
			rowView = inflater.inflate(R.layout.spinner_item, null);
			
			TextView tv = (TextView)rowView.findViewById(R.id.spinnerItem);
			
			tv.setText(listDevices.get(position).getName());
		}
		return rowView;
	}

}
