package com.medic.main;

import com.medic.database.HelperFactory;
import com.medic.fftshow.R;

import android.app.Application;

public class ApplicationMain extends Application {

	@Override
	public void onCreate() {	
		super.onCreate();
		HelperFactory.SetHelper(getApplicationContext());
	}
	
	@Override
	public void onTerminate() {
	    HelperFactory.ReleaseHelper();
		super.onTerminate();
	}

}
