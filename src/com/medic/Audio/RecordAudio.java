package com.medic.Audio;

import com.medic.demodulator.Decoder;
import com.medic.graphics.XYChartBuilder;

import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.os.AsyncTask;
import android.os.Process;
import android.util.Log;

public class RecordAudio extends AsyncTask<Void, short[], Void> {
	
	private double startTime = System.currentTimeMillis();

	private double curTime = System.currentTimeMillis();
	
	private short prev_num;
	
	private Decoder decoder = new Decoder();
	
	private final int Fs = 8000;
	
	//private boolean mIsRunning;

	@Override
	protected Void doInBackground(Void... params) {
		
		Process.setThreadPriority(Process.THREAD_PRIORITY_URGENT_AUDIO);
		
		
		int BUFF_COUNT = 2048;
		
		int buffSize = AudioRecord.getMinBufferSize(Fs,
				AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT);

		
		short[][] buffers = new short[BUFF_COUNT][buffSize >> 1];

		AudioRecord mRecord = new AudioRecord(MediaRecorder.AudioSource.MIC,
				Fs, AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT,
				 buffSize * 10);

		
		mRecord.startRecording();

		int count = 0;

		while (XYChartBuilder.RecordRunning) {

			int samplesRead = mRecord.read(buffers[count], 0,
						buffers[count].length);
			short[] num = new short[1];
			num[0] = (short)count;
			publishProgress(buffers[count], num);

			count = (count + 1) % BUFF_COUNT;
		}
		return null;
	}

	protected void onProgressUpdate(short[]... toTransform) {
		Log.e("RecordingProgress", "Displaying in progress");
		
		long begin_time = System.currentTimeMillis();
		short cur_num = toTransform[1][0];
		if ((cur_num-prev_num)==1){
		for (int i = 0; i < toTransform[0].length; i++) {
			
			double decoded = decoder.decode((double) toTransform[0][i]/32768);
			
			if (i % 30 == 0)
				XYChartBuilder.AddNewPoint((curTime - startTime) / (double) 1000, -decoded);

			curTime += 1000 / (double) Fs;
			
			
		}
		}
		prev_num=cur_num;
		
		long diff_time = System.currentTimeMillis() - begin_time;
		
		Log.d("Proccess time", diff_time+" ms");

	}
}